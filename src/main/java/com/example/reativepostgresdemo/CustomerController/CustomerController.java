package com.example.reativepostgresdemo.CustomerController;

import com.example.reativepostgresdemo.Model.Customer;
import com.example.reativepostgresdemo.Repo.CustomerRepo;
import lombok.Builder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/customer")
public class CustomerController {


   public CustomerRepo repository;

    public CustomerController(CustomerRepo repository) {
        this.repository = repository;
    }

    @GetMapping
    public Flux<Customer> getAllCustomers() {
        return repository.findAll();
    }
    @GetMapping("/{id}")
    public Mono<Customer> getCustomer(@PathVariable Integer id) {
        return repository.findById(id);
    }

    @PostMapping
    public Mono<Customer> createCustomer(@PathVariable Customer customer) {
        return repository.save(customer);
    }

    @PutMapping("/{id}")
    public Mono<Customer> updateCustomer(@PathVariable Customer customer, @PathVariable Integer id) {
        return repository.findById(id)
                .map(c->{
                    c.setName(customer.getName());
                    return c;
                }).flatMap(c-> repository.save(c));
    }

    @DeleteMapping("/{id}")
    public Mono<Void> createCustomer( @PathVariable Integer id) {
        return repository.deleteById(id);
    }

}
