package com.example.reativepostgresdemo.Repo;

import com.example.reativepostgresdemo.Model.Customer;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
//@EnableR2dbcRepositories
public interface CustomerRepo extends ReactiveCrudRepository<Customer,Integer> {
}
